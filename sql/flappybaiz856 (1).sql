CREATE DATABASE ppeself;
use ppeself;
-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : flappybaiz856.mysql.db
-- Généré le :  sam. 27 avr. 2019 à 21:59
-- Version du serveur :  5.6.39-log
-- Version de PHP :  7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `flappybaiz856`
--

-- --------------------------------------------------------

--
-- Structure de la table `menu_produit`
--

CREATE TABLE `menu_produit` (
  `designation` varchar(45) NOT NULL,
  `type` varchar(14) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `menu_produit`
--

INSERT INTO `menu_produit` (`designation`, `type`, `description`) VALUES
('Ananas frais', 'dessert', NULL),
('Banane', 'dessert', NULL),
('Betteraves aux oignons', 'accompagnement', NULL),
('Cari de bœuf', 'plat', NULL),
('Carottes râpées au thon', 'entree', NULL),
('Carré frais', 'laitage', NULL),
('Céleri rémoulade', 'entree', NULL),
('Clémentines', 'dessert', NULL),
('Colin à la bordelaise', 'plat', NULL),
('Compote pommes framboises', 'dessert', NULL),
('Concombre à la ciboulette', 'entree', NULL),
('Coquelet', 'plat', NULL),
('Cordon bleu', 'plat', NULL),
('Crumble de carottes', 'accompagnement', NULL),
('Crumble de courgettes', 'accompagnement', NULL),
('Danette', 'dessert', NULL),
('Edam', 'laitage', NULL),
('Emincé de bœuf', 'plat', NULL),
('Encornet à la romaine', 'plat', NULL),
('Epinards à la crème', 'accompagnement', NULL),
('Escalope viennoise', 'plat', NULL),
('Filet de julienne', 'plat', NULL),
('Fraise au sucre', 'dessert', NULL),
('Fromage blanc', 'laitage', NULL),
('Gouda', 'laitage', NULL),
('Gratin de choux fleurs', 'accompagnement', NULL),
('Haricots \"beurre\"', 'accompagnement', NULL),
('Haricots verts', 'accompagnement', NULL),
('Kiwi', 'dessert', NULL),
('Liégeois', 'dessert', NULL),
('Macaroni', 'accompagnement', NULL),
('Mi chèvre', 'laitage', NULL),
('Mimolette', 'laitage', NULL),
('Mousse de canard', 'entree', NULL),
('Pastèque', 'entree', NULL),
('Pêches au sirop', 'dessert', NULL),
('Petits pois', 'accompagnement', NULL),
('Pomme verte', 'dessert', NULL),
('Pont l\'évèque', 'laitage', NULL),
('Ratatouille', 'accompagnement', NULL),
('Riz pilaf', 'accompagnement', NULL),
('Rondelé', 'laitage', NULL),
('Salade de blé au thon', 'entree', NULL),
('Salade de fusilli', 'entree', NULL),
('Salade piémontaise', 'entree', NULL),
('Salade verte à composer', 'entree', 'un peu de tout'),
('Steak haché', 'plat', NULL),
('Steak végétal', 'plat', NULL),
('Tagliatelle à l\'italienne', 'entree', NULL),
('Terrine de poisson', 'entree', NULL),
('Tomates mozarella', 'entree', NULL),
('Yaourt sucré', 'laitage', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `menu_repas`
--

CREATE TABLE `menu_repas` (
  `date` date NOT NULL,
  `type` varchar(8) NOT NULL,
  `text_special` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `menu_repas`
--

INSERT INTO `menu_repas` (`date`, `type`, `text_special`) VALUES
('2019-04-29', 'dejeuner', NULL),
('2019-04-29', 'diner', NULL),
('2019-04-30', 'dejeuner', NULL),
('2019-05-02', 'dejeuner', NULL),
('2019-05-02', 'diner', NULL),
('2019-05-03', 'dejeuner', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `menu_repas_produit`
--

CREATE TABLE `menu_repas_produit` (
  `date` date NOT NULL,
  `type_repas` varchar(8) NOT NULL,
  `designation` varchar(45) NOT NULL,
  `type_produit` varchar(14) NOT NULL,
  `bio` tinyint(1) DEFAULT '0',
  `local` tinyint(1) DEFAULT '0',
  `maison` tinyint(1) DEFAULT '0',
  `frais` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `menu_repas_produit`
--

INSERT INTO `menu_repas_produit` (`date`, `type_repas`, `designation`, `type_produit`, `bio`, `local`, `maison`, `frais`) VALUES
('2019-04-29', 'dejeuner', 'Céleri rémoulade', 'entree', 0, 0, 1, 0),
('2019-04-29', 'dejeuner', 'Danette', 'dessert', 0, 0, 0, 0),
('2019-04-29', 'dejeuner', 'Edam', 'laitage', 0, 0, 0, 0),
('2019-04-29', 'dejeuner', 'Encornet à la romaine', 'plat', 0, 0, 0, 0),
('2019-04-29', 'dejeuner', 'Haricots verts', 'accompagnement', 0, 0, 1, 0),
('2019-04-29', 'dejeuner', 'Liégeois', 'laitage', 0, 0, 0, 0),
('2019-04-29', 'dejeuner', 'Pont l\'évèque', 'laitage', 0, 0, 0, 0),
('2019-04-29', 'dejeuner', 'Riz pilaf', 'accompagnement', 0, 0, 1, 0),
('2019-04-29', 'dejeuner', 'Salade verte à composer', 'entree', 0, 0, 0, 0),
('2019-04-29', 'dejeuner', 'Steak végétal', 'plat', 0, 0, 0, 0),
('2019-04-29', 'dejeuner', 'Tomates mozarella', 'entree', 0, 0, 1, 0),
('2019-04-29', 'diner', 'Concombre à la ciboulette', 'entree', 0, 0, 1, 0),
('2019-04-29', 'diner', 'Gouda', 'laitage', 0, 0, 0, 0),
('2019-04-29', 'diner', 'Petits pois', 'accompagnement', 0, 0, 0, 0),
('2019-04-29', 'diner', 'Pomme verte', 'dessert', 0, 0, 0, 0),
('2019-04-29', 'diner', 'Salade de fusilli', 'entree', 0, 0, 1, 0),
('2019-04-29', 'diner', 'Steak haché', 'plat', 0, 0, 0, 0),
('2019-04-30', 'dejeuner', 'Coquelet', 'plat', 0, 0, 0, 0),
('2019-04-30', 'dejeuner', 'Crumble de carottes', 'accompagnement', 0, 0, 1, 0),
('2019-04-30', 'dejeuner', 'Escalope viennoise', 'plat', 0, 0, 0, 0),
('2019-04-30', 'dejeuner', 'Fraise au sucre', 'dessert', 0, 0, 0, 0),
('2019-04-30', 'dejeuner', 'Gratin de choux fleurs', 'accompagnement', 0, 0, 1, 0),
('2019-04-30', 'dejeuner', 'Kiwi', 'dessert', 0, 0, 0, 0),
('2019-04-30', 'dejeuner', 'Mousse de canard', 'entree', 0, 0, 0, 0),
('2019-04-30', 'dejeuner', 'Salade de blé au thon', 'entree', 0, 0, 1, 0),
('2019-04-30', 'dejeuner', 'Salade verte à composer', 'entree', 0, 0, 0, 0),
('2019-04-30', 'dejeuner', 'Yaourt sucré', 'laitage', 0, 0, 0, 0),
('2019-05-02', 'dejeuner', 'Carottes râpées au thon', 'entree', 0, 0, 1, 0),
('2019-05-02', 'dejeuner', 'Carré frais', 'laitage', 0, 0, 0, 0),
('2019-05-02', 'dejeuner', 'Colin à la bordelaise', 'plat', 0, 0, 0, 0),
('2019-05-02', 'dejeuner', 'Compote pommes framboises', 'dessert', 0, 1, 1, 0),
('2019-05-02', 'dejeuner', 'Epinards à la crème', 'accompagnement', 0, 0, 1, 0),
('2019-05-02', 'dejeuner', 'Filet de julienne', 'plat', 0, 0, 1, 0),
('2019-05-02', 'dejeuner', 'Macaroni', 'accompagnement', 0, 0, 0, 0),
('2019-05-02', 'dejeuner', 'Mimolette', 'laitage', 0, 0, 0, 0),
('2019-05-02', 'dejeuner', 'Pastèque', 'entree', 0, 0, 0, 0),
('2019-05-02', 'dejeuner', 'Pêches au sirop', 'dessert', 0, 0, 0, 0),
('2019-05-02', 'dejeuner', 'Salade verte à composer', 'entree', 0, 0, 0, 0),
('2019-05-02', 'diner', 'Ananas frais', 'dessert', 0, 0, 0, 0),
('2019-05-02', 'diner', 'Betteraves aux oignons', 'entree', 0, 0, 1, 0),
('2019-05-02', 'diner', 'Cordon bleu', 'plat', 0, 0, 0, 0),
('2019-05-02', 'diner', 'Crumble de courgettes', 'accompagnement', 0, 0, 1, 0),
('2019-05-02', 'diner', 'Fromage blanc', 'laitage', 0, 0, 0, 0),
('2019-05-02', 'diner', 'Salade piémontaise', 'entree', 0, 0, 1, 0),
('2019-05-03', 'dejeuner', 'Banane', 'dessert', 0, 0, 0, 0),
('2019-05-03', 'dejeuner', 'Cari de bœuf', 'plat', 0, 0, 1, 0),
('2019-05-03', 'dejeuner', 'Clémentines', 'dessert', 0, 0, 0, 0),
('2019-05-03', 'dejeuner', 'Emincé de bœuf', 'plat', 0, 0, 1, 0),
('2019-05-03', 'dejeuner', 'Haricots \"beurre\"', 'accompagnement', 0, 0, 1, 0),
('2019-05-03', 'dejeuner', 'Mi chèvre', 'laitage', 0, 0, 0, 0),
('2019-05-03', 'dejeuner', 'Ratatouille', 'accompagnement', 0, 0, 1, 0),
('2019-05-03', 'dejeuner', 'Rondelé', 'laitage', 0, 0, 0, 0),
('2019-05-03', 'dejeuner', 'Salade verte à composer', 'entree', 0, 0, 0, 0),
('2019-05-03', 'dejeuner', 'Tagliatelle à l\'italienne', 'entree', 0, 0, 1, 0),
('2019-05-03', 'dejeuner', 'Terrine de poisson', 'entree', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `menu_type_produit`
--

CREATE TABLE `menu_type_produit` (
  `type` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `menu_type_produit`
--

INSERT INTO `menu_type_produit` (`type`) VALUES
('accompagnement'),
('dessert'),
('entree'),
('laitage'),
('plat');

-- --------------------------------------------------------

--
-- Structure de la table `menu_type_repas`
--

CREATE TABLE `menu_type_repas` (
  `type` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `menu_type_repas`
--

INSERT INTO `menu_type_repas` (`type`) VALUES
('dejeuner'),
('diner');

-- --------------------------------------------------------

--
-- Structure de la table `user_member`
--

CREATE TABLE `user_member` (
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `note` text,
  `enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user_member`
--

INSERT INTO `user_member` (`login`, `password`, `name`, `date_creation`, `note`, `enable`) VALUES
('adminmenu', '3ed7dceaf266cafef032b9d5db224717', 'Monsieur Menu', '2019-04-23 20:12:49', NULL, 1),
('chewby', 'e2bc35b4af3240d45e63e0d864a43ea1', 'Remi Bourgeois', '2019-03-20 18:08:16', NULL, 1),
('flappyBat', '493a2cf662c600f909cb73f9bbe88c15', 'Antoine Battelier', '2019-03-14 20:35:53', NULL, 1),
('test', '098f6bcd4621d373cade4e832627b4f6', 'Je suis test', '2019-04-23 21:49:04', NULL, 1),
('zermelwall@gmail.com', '5a5d0ef45e0d4acf1fdb701b29ae49e1', 'Brandon Zermelwall', '2019-03-28 15:19:16', 'mdp cochon d\'inde', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_permission`
--

CREATE TABLE `user_permission` (
  `member` varchar(45) NOT NULL DEFAULT '',
  `permission` varchar(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user_permission`
--

INSERT INTO `user_permission` (`member`, `permission`) VALUES
('flappyBat', 'admin'),
('chewby', 'lol19'),
('test', 'lol19'),
('adminmenu', 'menu'),
('test', 'menu'),
('zermelwall@gmail.com', 'menu');

-- --------------------------------------------------------

--
-- Structure de la table `user_permission_type`
--

CREATE TABLE `user_permission_type` (
  `type` varchar(5) NOT NULL,
  `lib` varchar(45) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user_permission_type`
--

INSERT INTO `user_permission_type` (`type`, `lib`, `description`) VALUES
('admin', 'Administrateur', NULL),
('lol19', 'Modérateur tournois LoL 2019', NULL),
('menu', 'Menueur', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `menu_produit`
--
ALTER TABLE `menu_produit`
  ADD PRIMARY KEY (`designation`,`type`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `menu_repas`
--
ALTER TABLE `menu_repas`
  ADD PRIMARY KEY (`date`,`type`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `menu_repas_produit`
--
ALTER TABLE `menu_repas_produit`
  ADD PRIMARY KEY (`date`,`type_repas`,`designation`,`type_produit`),
  ADD KEY `type_repas` (`type_repas`),
  ADD KEY `designation` (`designation`),
  ADD KEY `type_produit` (`type_produit`);

--
-- Index pour la table `menu_type_produit`
--
ALTER TABLE `menu_type_produit`
  ADD PRIMARY KEY (`type`);

--
-- Index pour la table `menu_type_repas`
--
ALTER TABLE `menu_type_repas`
  ADD PRIMARY KEY (`type`);

--
-- Index pour la table `user_member`
--
ALTER TABLE `user_member`
  ADD PRIMARY KEY (`login`);

--
-- Index pour la table `user_permission`
--
ALTER TABLE `user_permission`
  ADD PRIMARY KEY (`member`,`permission`),
  ADD KEY `permission` (`permission`);

--
-- Index pour la table `user_permission_type`
--
ALTER TABLE `user_permission_type`
  ADD PRIMARY KEY (`type`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `menu_produit`
--
ALTER TABLE `menu_produit`
  ADD CONSTRAINT `menu_produit_ibfk_1` FOREIGN KEY (`type`) REFERENCES `menu_type_produit` (`type`);

--
-- Contraintes pour la table `menu_repas`
--
ALTER TABLE `menu_repas`
  ADD CONSTRAINT `menu_repas_ibfk_1` FOREIGN KEY (`type`) REFERENCES `menu_type_repas` (`type`);

--
-- Contraintes pour la table `menu_repas_produit`
--
ALTER TABLE `menu_repas_produit`
  ADD CONSTRAINT `menu_repas_produit_ibfk_1` FOREIGN KEY (`date`) REFERENCES `menu_repas` (`date`),
  ADD CONSTRAINT `menu_repas_produit_ibfk_2` FOREIGN KEY (`type_repas`) REFERENCES `menu_repas` (`type`),
  ADD CONSTRAINT `menu_repas_produit_ibfk_3` FOREIGN KEY (`designation`) REFERENCES `menu_produit` (`designation`),
  ADD CONSTRAINT `menu_repas_produit_ibfk_4` FOREIGN KEY (`type_produit`) REFERENCES `menu_produit` (`type`);

--
-- Contraintes pour la table `user_permission`
--
ALTER TABLE `user_permission`
  ADD CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`member`) REFERENCES `user_member` (`login`),
  ADD CONSTRAINT `user_permission_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `user_permission_type` (`type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
